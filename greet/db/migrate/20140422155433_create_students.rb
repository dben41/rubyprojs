class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.float :gpa
      t.integer :uid

      t.timestamps
    end
  end
end
