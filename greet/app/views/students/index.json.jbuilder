json.array!(@students) do |student|
  json.extract! student, :id, :name, :gpa, :uid
  json.url student_url(student, format: :json)
end
