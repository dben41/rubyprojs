#!/spec/controllers/songs_controller_spec.rb
=begin
Test cases written foo our songs controller. These tests
ensure that proper routing takes place. These also ensure
that the corresponding pages exist and are not empty.

@author Taylor Wilson
@author Daryl Bennett
=end

require 'spec_helper'

describe SongsController do

  # if the pages are not rendered rspec cannot find
  # elements within the rendered page
  render_views

  # validate route to songs/index.html.erb
  describe "GET 'song'" do
    it "should be successful" do
      get 'index'
      response.should be_success
    end
    
    # validate title
    it "should have the right title" do
      get 'index'
      response.should have_selector("title", :content => "Forum")
    end

    # validate non-empty body
    it "should have a non-empty body" do
      get 'index'
      response.body.should_not =~ /<body>\s*<\/body>/
    end

    # validate the existence of a table on the index page
    it "should have 1 table" do
      get 'index'
      response.should have_selector("table", :count => 1)
    end
  end

  # validate route to songs/new
  describe "GET 'song'" do
    it "should be successful" do
      get 'new'
      response.should be_success
    end

    # validate title
    it "should have the right title" do
      get 'new'
      response.should have_selector("title", :content => "Forum")
    end

    # validate non-empty body
    it "should have a non-empty body" do
      get 'new'
      response.body.should_not =~ /<body>\s*<\/body>/
    end

    # validate the existence of a form
    it "should have 1 form" do
      get 'new'
      response.should have_selector("form", :count => 1)
    end    

    # validate form structure
    it "should have 5 labels in the form" do
      get 'new'
      response.should have_selector("label", :count => 5)
    end

    # validate form structure
    it "should have 6 inputs" do
      get 'new'
      response.should have_selector("input", :count => 6)
    end
  end
end
