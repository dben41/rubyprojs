#!/spec/controllers/pages_controller_spec.rb                                                              
=begin
Test cases written foo our songs controller. These tests 
ensure that proper routing takes place. These also ensure
that the corresponding pages exist and are not empty.

@author Taylor Wilson
@author Daryl Bennett                                                                                     
=end

require 'spec_helper'

describe PagesController do

  # if the pages are not rendered rspec cannot find
  # elements within the rendered page  
  render_views

  # validate route to pages/home.html.erb
  describe "GET 'home'" do
    it "should be successful" do
      get 'home'
      response.should be_success
    end

    # validate title
    it "should have the right title" do
      get 'home'
      response.should have_selector("title", :content => "Home")
    end

    # validate existence of navigation bar
    it "should have a navigation bar" do
      get 'home'
      response.body.should have_selector("section", :count => 1);
    end

    # validate non-empty body
    it "should have a non-empty body" do
      get 'home'
      response.body.should_not =~ /<body>\s*<\/body>/
    end
  end

  # validate route to pages/about.html.erb
  describe "GET 'about'" do
    it "should be successful" do
      get 'about'
      response.should be_success
    end

    # validate title
    it "should have the right title" do
      get 'about'
      response.should have_selector("title", :content => "About Us")
    end

    # validate existence of navigation bar
   it "should have a navigation bar" do
      get 'about'
      response.body.should have_selector("section", :count => 1);
    end

    # validate non-empty body
    it "should have a non-empty body" do
      get 'about'
      response.body.should_not =~ /<body>\s*<\/body>/
    end
  end

  # validate route to pages/contact.html.erb
  describe "GET 'contact'" do
    it "should be successful" do
      get 'contact'
      response.should be_success
    end

    # validate title
    it "should have the right title" do
      get 'contact'
      response.should have_selector("title", :content => "Contact Us")
    end

    # validate existence of navigation bar
    it "should have a navigation bar" do
      get 'contact'
      response.body.should have_selector("section", :count => 1);
    end

    # validate non-empty body
    it "should have a non-empty body" do
      get 'contact'
      response.body.should_not =~ /<body>\s*<\/body>/
    end
  end

  # validate route to pages/help.html.erb
  describe "GET 'help'" do
    it "should be successful" do
      get 'help'
      response.should be_success
    end
    
    # validate title
    it "should have the right title" do
      get 'help'
      response.should have_selector("title", :content => "Help")
    end

    # validate existence of navigation bar
    it "should have a navigation bar" do
      get 'help'
      response.body.should have_selector("section", :count => 1);
    end

    # validate non-empty body
    it "should have a non-empty body" do
      get 'help'
      response.body.should_not =~ /<body>\s*<\/body>/
    end
  end
end

