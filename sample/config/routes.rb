#!/config/routes.rb
=begin
Routing setup for the entire site.

@author Daryl Bennett
@author Taylor Wilson
=end

Rails.application.routes.draw do

  resources :songs do
    resources :comments
  end

  resources :users
  resources :sessions, only: [:new, :create, :destroy]

  #reroute default page
  root 'pages#home'

  #map the url's to certain links (matched route)
  match '/home',     :to => 'pages#home',    via: [:get, :post]
  match '/song',     :to => 'songs#index',   via: [:get, :post]
  match '/contact',  :to => 'pages#contact', via: [:get, :post]
  match '/about',    :to => 'pages#about',   via: [:get, :post]
  match '/help',     :to => 'pages#help',    via: [:get, :post]
  match '/login',    :to => 'pages#login',   via: [:get, :post]
  match '/admin',    :to => 'pages#admin',   via: [:get, :post]
  match '/register', :to => 'users#new',     via: [:get, :post]

  #these are the default URL's
  #get 'pages/home'
  #get 'pages/contact'
  #get 'pages/about'
  #get 'pages/help'
  #get 'pages/login'
  #get 'pages/admin'
  #get 'pages/register'
end
