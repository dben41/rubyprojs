class AddEmailUniquenessIndex < ActiveRecord::Migration
  #ensures uniqueness constraint
  def change
  	add_index :users, :email, :unique => true
  end
end
