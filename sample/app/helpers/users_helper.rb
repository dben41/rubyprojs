#!app/helper/users_helper.rb
=begin
Display a gravatar for a user's profile.
@author Daryl Bennett
@author Taylor Wilson

Users helper is modeled after Michael Hartl's screencast tutorial
on applying gravatars to users.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end

module UsersHelper
	
  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, options = { size: 50 })
   	gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
end
