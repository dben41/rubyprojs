#!app/helper/session_helper.rb
=begin
@author Daryl Bennett
@author Taylor Wilson

Sessions helper is modeled after Michael Hartl's screencast tutorial
on working with sessions.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end

module SessionsHelper
	 def sign_in(user)
		remember_token = User.new_remember_token
		cookies.permanent[:remember_token] = remember_token
		user.update_attribute(:remember_token, User.digest(remember_token))
		self.current_user = user
	  end
	  
	  #the correct way to get user
	  def current_user=(user)
    	@current_user = user
  	  end
  	 
  	  def signed_in?
		!current_user.nil?
	  end
  	  
  	  def current_user
		remember_token = User.digest(cookies[:remember_token])
		#only sets if undefined
		@current_user ||= User.find_by(remember_token: remember_token)
	  end
end
