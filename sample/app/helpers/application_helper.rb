#!app/helper/application_helper.rb
=begin
@author Daryl Bennett
@author Taylor Wilson

Application helper is modeled after Michael Hartl's screencast tutorial
on creating dynamic titles.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end

module ApplicationHelper

	#Dynamically populates page title
	def title
		base_title = "Jam!"
		if @title.nil?
			base_title
		else
			"#{base_title} | #{@title}"
		end
	end	
end
