#!app/models/user.rb
=begin

Schema Info

Table name: users

id			:integer		not null, primary key
name		:string(255)	user's name
email		:string(255)    unique
created_at  :datetime
updated_at	:datetime
encrypted_password: string

@author Daryl Bennett
@author Taylor Wilson

User model is modeled after Michael Hartl's screencast tutorial
on setting up a user model.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end

class User < ActiveRecord::Base
	has_secure_password	#uses the authenticate method
	before_save {self.email = email.downcase }	
	before_create :create_remember_token
	#alterable fields within database
	#attr_accessible :name, :email, :password, :password_confirmation
	attr_accessor	:password, :password_confirmation
	
	#http://rubular.com/ 
	email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	
	#ensures the form fields are not empty, 
	#use correct length, uniqueness and format is correct
	validates :name,  :presence     => true,
					  :length       => { :maximum =>40 }
	validates :email, :presence     => true,
					  :format       => {:with => email_regex },
					  :uniqueness   => {:case_sensitive => false }
	validates  :password, 
					  :presence     => true, 
					  :confirmation => true,
					  :length       => { :within => 6..40 }
	
	#this is temporary
	#validates :encrypted_password, :presence     => true

   def User.new_remember_token
     SecureRandom.urlsafe_base64
   end

   def User.encrypt(token)
     Digest::SHA1.hexdigest(token.to_s)
   end
  
   def User.digest(token)
     Digest::SHA1.hexdigest(token.to_s)
   end
  
   #has_secure_password
    
   private

     def create_remember_token
       self.remember_token = User.encrypt(User.new_remember_token)
     end					  
end
