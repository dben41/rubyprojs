class Song < ActiveRecord::Base
  has_many :comments
  validates :title,       presence: true
  validates :date,        presence: true
  validates :description, presence: true
  validates :lyrics,      presence: true

end
