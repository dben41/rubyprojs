#!app/controller/songs_controller.rb
=begin
@author Taylor Wilson
@author Daryl Bennett

Controller for song objects. This controller is based on the controller 
created in the 'Getting Started' tutorial.
http://guides.rubyonrails.org/getting_started.html
=end

class CommentsController < ApplicationController

  def create
    @song = Song.find(params[:song_id])
    @comment = @song.comments.create(comment_params)
    redirect_to song_path(@song)
  end

  def destroy
    @song = Song.find(params[:song_id])
    @song = @song.comments.find(params[:id])
    @comment.destroy
    redirect_to song_path(@song)
  end
 
  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end
