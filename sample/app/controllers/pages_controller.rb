#!app/controller/pages_controller.rb
=begin
@author Daryl Bennett
@author Taylor Wilson

Pages controller is modeled after Michael Hartl's screencast tutorial
on setting up page controls.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end
class PagesController < ApplicationController

  http_basic_authenticate_with name: "admin", password: "admin", only: [:admin]

  def home
  	@title = "Home"
  end

  def song
    @title = "Song"
  end 

  def contact
  	@title = "Contact Us"
  end
  
  def about
   	@title = "About Us"
  end
  
  def register
   	@title = "Register"
  end
  
  def admin
   	@title = "Admin"
  end
  
  def login
   	@title = "Login"
  end
  
  def help
   	@title = "Help"
  end
end
