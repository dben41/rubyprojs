#!app/controller/songs_controller.rb
=begin
@author Taylor Wilson
@author Daryl Bennett

Controller for song objects. This controller is based on the controller 
created in the 'Getting Started' tutorial.
http://guides.rubyonrails.org/getting_started.html
=end

class SongsController < ApplicationController

  http_basic_authenticate_with name: "admin", password: "admin", only: :destroy

  def index
    @songs = Song.all
    @title = "Forum"
  end

  def new
    @song  = Song.new
    @title = "Forum"
  end
 
  def create
    @song = Song.new(song_params)
 
    if @song.save
      redirect_to @song
    else
      render 'new'
    end
  end

  def show
    @song = Song.find(params[:id])
    @title = "Forum"
  end

  def edit
    @song = Song.find(params[:id])
    @title = "Forum"
  end

  def update
    @song = Song.find(params[:id])

    if @song.update(song_params)
      redirect_to @song
    else
      render 'edit'
    end
  end

  def destroy
    @song = Song.find(params[:id])
    @song.destroy

    redirect_to song_path
  end
 
  private
    def song_params
      params.require(:song).permit(:title, :date, :description, :link, :lyrics)
    end
end
