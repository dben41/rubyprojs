#!app/controller/user_controller.rb
=begin
@author Daryl Bennett
@author Taylor Wilson

User controller is modeled after Michael Hartl's screencast tutorial
on setting up user controls.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end

class UsersController < ApplicationController
  def new
  	@title = "Sign up"
  	@user = User.new
  end

  #display user profile
  def show
  	@user = User.find(params[:id])
  	@title = @user.name 
  end
  
  #security dealt wtih
	def create
	  @user = User.new(user_params)
	  if @user.save
		redirect_to @user, :flash => { :success => "Welcome to Jam!" }
	  else
		@title = "Sign up"
		render 'new'
	  end
	end

  private
  def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
end
