#!app/controller/session_controller.rb
=begin
@author Daryl Bennett
@author Taylor Wilson

Session controller is modeled after Michael Hartl's screencast tutorial
on setting up session controls.
https://www.youtube.com/watch?v=nFQ_ulWiCCI&list=PLVBFw0Pn9e9L7SOKtL8x4Av39drO5Oi-Q
=end

class SessionsController < ApplicationController

 def new
  end

  def create
   user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Sign the user in and redirect to the user's show page.
      sign_in user
      redirect_to user
    else
      flash.now[:error] = 'Invalid email/password combination' # Not quite right!
  	render 'new'
  	end
  end

  def destroy
  	logout
    redirect_to root_url
  end
  
end
